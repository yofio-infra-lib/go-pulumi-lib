module gitlab.com/yofio-infra-lib/go-pulumi-lib

go 1.14

require (
	github.com/pulumi/pulumi-docker/sdk/v2 v2.2.3 // indirect
	github.com/pulumi/pulumi-gcp/sdk/v3 v3.10.1 // indirect
	github.com/pulumi/pulumi/sdk/v2 v2.4.0 // indirect
)
