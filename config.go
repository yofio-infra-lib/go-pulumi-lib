package lib

import (
	"fmt"

	"github.com/pulumi/pulumi-docker/sdk/v2/go/docker"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/cloudrun"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/dns"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/organizations"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/serviceAccount"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
)

// Constants for GCP deployment process
const (
	GcrRegistry         = "gcr.io"
	CloudRunRole        = "roles/run.invoker"
	ImgRegistryUsername = "oauth2accesstoken"
	GcpDNSHostName      = "ghs.googlehosted.com."
	CName               = "CNAME"
	TimeToLive          = 300
	DockerEnvKey        = "env_value"
)

// MustGcpProjectID returns GCP project ID
func MustGcpProjectID(ctx *pulumi.Context) string {
	gcpProjectID, ok := ctx.GetConfig("gcp:project")
	if !ok {
		panic("gcp project id is undefined")
	}
	return gcpProjectID
}

// MustGcpRegion returns GCP region
func MustGcpRegion(ctx *pulumi.Context) string {
	gcpRegion, ok := ctx.GetConfig("gcp:region")
	if !ok {
		panic("gcp region is undefined")
	}
	return gcpRegion
}

// MustCustomConfig returns a custom property value from config
func MustCustomConfig(ctx *pulumi.Context, configKey string) string {
	customConfig, ok := ctx.GetConfig(configKey)
	if !ok {
		panic(fmt.Sprintf("%s custom config property is undefined", configKey))
	}
	return customConfig
}

// GetServiceAccount search for a service account and return the email
func GetServiceAccount(ctx *pulumi.Context, gcpProjectID, serviceAccountID string) (string, error) {
	account, err := serviceAccount.LookupAccount(ctx, &serviceAccount.LookupAccountArgs{
		Project:   &gcpProjectID,
		AccountId: serviceAccountID,
	})
	if err != nil {
		return "", err
	}
	return account.Email, nil
}

// NewDNSRecord creates a new DNS recordset with given sub-domain
func NewDNSRecord(ctx *pulumi.Context, serviceName, gcpProjectID, gcpDNSManagedZone,
	subDomain string) (*dns.RecordSet, error) {

	dnsMZ, err := dns.LookupManagedZone(ctx, &dns.LookupManagedZoneArgs{
		Project: &gcpProjectID,
		Name:    gcpDNSManagedZone,
	})
	if err != nil {
		return nil, err
	}

	dnsRS, err := dns.NewRecordSet(ctx, fmt.Sprintf("%s-record-set", serviceName), &dns.RecordSetArgs{
		ManagedZone: pulumi.String(dnsMZ.Name),
		Name:        pulumi.String(fmt.Sprintf("%s.", subDomain)),
		Project:     pulumi.String(gcpProjectID),
		Rrdatas: pulumi.StringArray{
			pulumi.String(GcpDNSHostName),
		},
		Ttl:  pulumi.Int(TimeToLive),
		Type: pulumi.String(CName),
	})
	if err != nil {
		return nil, err
	}
	return dnsRS, nil
}

// DockerBuildAndPublishImg build a new docker image and publish it int GCR
func DockerBuildAndPublishImg(ctx *pulumi.Context, serviceName, gcpProjectID,
	dockerFilePath string) (pulumi.StringOutput, error) {

	imgName := fmt.Sprintf("%s/%s/%s:latest", GcrRegistry, gcpProjectID, serviceName)
	clientConfig, err := organizations.GetClientConfig(ctx)
	if err != nil {
		return pulumi.StringOutput{}, err
	}

	dockerImg, err := docker.NewImage(ctx, fmt.Sprintf("%s-docker-img", serviceName), &docker.ImageArgs{
		ImageName: pulumi.String(imgName),
		Build: docker.DockerBuildArgs{
			Context: pulumi.String(dockerFilePath),
		},
		Registry: docker.ImageRegistryArgs{
			Server:   pulumi.String(fmt.Sprintf("https://%s", GcrRegistry)),
			Username: pulumi.String(ImgRegistryUsername),
			Password: pulumi.String(clientConfig.AccessToken),
		},
	})
	if err != nil {
		return pulumi.StringOutput{}, err
	}
	return dockerImg.ImageName, nil
}

// DeployCloudRun deploys the docker image into cloud run service
func DeployCloudRun(ctx *pulumi.Context, serviceName, gcpProjectID, gcpRegion, serviceAccountEmail,
	dockerEnvValue, sqlCloudName string, imageName pulumi.StringOutput) (*cloudrun.Service, error) {

	var metadata cloudrun.ServiceTemplateMetadataPtrInput

	if sqlCloudName != "" {
		metadata = cloudrun.ServiceTemplateMetadataArgs{
			Annotations: pulumi.StringMap{
				"run.googleapis.com/cloudsql-instances": pulumi.Sprintf("%s:%s:%s", gcpProjectID, gcpRegion, sqlCloudName),
			},
		}
	}

	service, err := cloudrun.NewService(ctx, serviceName, &cloudrun.ServiceArgs{
		Location: pulumi.String(gcpRegion),
		Template: cloudrun.ServiceTemplateArgs{
			Metadata: metadata,
			Spec: cloudrun.ServiceTemplateSpecArgs{
				Containers: cloudrun.ServiceTemplateSpecContainerArray{
					cloudrun.ServiceTemplateSpecContainerArgs{
						Image: imageName,
						Envs: cloudrun.ServiceTemplateSpecContainerEnvArray{
							cloudrun.ServiceTemplateSpecContainerEnvArgs{
								Name:  pulumi.StringPtr(DockerEnvKey),
								Value: pulumi.StringPtr(dockerEnvValue), // ctx.Stack()
							},
						},
					},
				},
				ServiceAccountName: pulumi.String(serviceAccountEmail),
			},
		},
	}, pulumi.DeleteBeforeReplace(true))
	if err != nil {
		return nil, err
	}

	_, err = cloudrun.NewIamMember(ctx, fmt.Sprintf("%s-iam-member", serviceName), &cloudrun.IamMemberArgs{
		Location: pulumi.String(gcpRegion),
		Service:  service.Name,
		Role:     pulumi.String(CloudRunRole),
		Member:   pulumi.String("allUsers"),
	})
	if err != nil {
		return nil, err
	}
	return service, nil
}

// NewDomainMapping creates a new domain mapping for the given cloud run service
func NewDomainMapping(ctx *pulumi.Context, serviceName, gcpProjectID, gcpRegion,
	subDomain string, service *cloudrun.Service, dnsRS *dns.RecordSet) error {

	_, err := cloudrun.NewDomainMapping(ctx, fmt.Sprintf("%s-domain-mapping", serviceName), &cloudrun.DomainMappingArgs{
		Location: pulumi.String(gcpRegion),
		Project:  pulumi.String(gcpProjectID),
		Name:     pulumi.String(subDomain),
		Metadata: cloudrun.DomainMappingMetadataArgs{
			Namespace: pulumi.String(gcpProjectID),
		},
		Spec: cloudrun.DomainMappingSpecArgs{
			RouteName: service.Name,
		},
	}, pulumi.DependsOn([]pulumi.Resource{dnsRS, service}))
	if err != nil {
		return err
	}
	return nil
}
